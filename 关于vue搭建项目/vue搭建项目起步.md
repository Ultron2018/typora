## 一, 第一部分

vue 引用elementUI 搭建简单的后台项目

如何安装vue

### 1.vue创建项目（npm安装→初始化项目）

第一步npm安装 首先：先从nodejs.org中下载nodejs

 ![img](https://upload-images.jianshu.io/upload_images/11329965-5a9ee3f9b2ca0d00.png?imageMogr2/auto-orient/strip|imageView2/2/w/741/format/webp) 

直到Finish完成安装。打开控制命令行程序（CMD）,检查是否正常

 ![img](https://upload-images.jianshu.io/upload_images/11329965-fb027342d30fdb7c.png?imageMogr2/auto-orient/strip|imageView2/2/w/766/format/webp) 

### 2.使用淘宝NPM镜像

大家都知道国内直接使用npm 的官方镜像是非常慢的，这里推荐使用淘宝 NPM 镜像。

$ npm install -g cnpm --registry=https://registry.npm.taobao.org

这样就可以使用cnpm 命令来安装模块了：

### 3.第二步项目初始化

##### 		a.第一步：安装vue-cli

​	 cnpm install vue-cli -g   //全局安装 vue-cli 

 查看vue-cli是否成功，不能检查vue-cli,需要检查vue 

​	vue list

![img](https://upload-images.jianshu.io/upload_images/11329965-ae7cd3b0e279c411.png?imageMogr2/auto-orient/strip|imageView2/2/w/822/format/webp) 



### 4.建立项目名称

 		vue init webpack "项目名称"

### 5.运行项目

cd  项目目录

​	vue install

​	cnpm run dev

引入element-UI

 cnpm i element-ui -S 

 main.js里面引入 elementUI 

```js
import ElementUI from 'element-ui';

import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
```

## 二 ,第二部分

自己创建项目

npm create "项目名"

npm run serve



首次下载项目代码

安装依赖

npm i

查看运行命令

type config.json



### 三, 图形化界面

vue ui

