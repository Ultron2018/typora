## 1. chrome浏览器下的xdebug helper使用方法

自从安装了xdebug后，发现每次调试都需要从eclipse中先从头启动，然后一步步走到你要调试的页面，而不是说想什么时候调试就什么时候调试。

之前用zenddebugger的时候则是可以在任意页面启动调试，直接从浏览器通知开发环境需要调试。而不用先从开发环境启动调试。随时需要调试的时候就可以执行调试。

后来发现了chrome浏览器有一款插件叫xdebug helper，火狐下也有easy xdebug，下面主要来说chrome下的xdebug helper

安装完成xdebug helper后再浏览器地址栏的右侧能够看到一只小爬虫，点击后如下图所示：

 ![20130224105419658](http://blog.zzstudio.net/wp-content/uploads/2014/04/20130224105419658.jpg) 



选择Debug，就会通知你的开发环境接下来的代码需要开始调试，选择disable,就会直接运行。

在eclipse中需要进行特别的设置：

进入window->Preferences->PHP->Debug
找到配置xdebug中的Accept remote session(JIT)，选择为localhost，并保存。

在PHP的配置文件中对xdebug的设置需要特别注意，将xdebug.remote_autostart设置为off，如果设置为on，则会忽略在浏览器中是选择Debug还是Disable，都会通知eclipse进行调试

xdebug.remote_autostart = Off
这样，xdebug helper就设置好了

以下是我的php.ini中对xdebug的配置



[Xdebug] ;xdebug配置

```ini
zend_extension="e:/php/ext/php_xdebug-2.2.1-5.4-vc9.dll" ;载入Xdebug

xdebug.profiler_enable=on

xdebug.trace_output_dir="e:/xdebug-log" ;xdebug 的数据文件目录

xdebug.profiler_output_dir="e:/xdebug-log" ;xdebug 的数据文件目录

xdebug.auto_trace = On ;开启自动跟踪

xdebug.show_exception_trace = On ;开启异常跟踪

xdebug.remote_autostart = Off ;开启远程调试自动启动

xdebug.remote_enable = On ;开启远程调试

xdebug.remote_handler=dbgp ;用于zend studio远程调试的应用层通信协议

xdebug.remote_host=127.0.0.1 ;允许连接的zend studio的IP地址

xdebug.remote_port=9000 ;反向连接zend studio使用的端口

xdebug.collect_vars = On ;收集变量

xdebug.collect_return = On ;收集返回值

xdebug.collect_params = On ;收集参数

xdebugbug.max_nesting_level = 10000 ;如果设得太小,函数中有递归调用自身次数太多时会报超过最大嵌套数错
```

作者：[风行天下](https://www.cnblogs.com/php-linux/)

出处：[https://www.cnblogs.com/brady-wang/](https://www.cnblogs.com/php-linux/)