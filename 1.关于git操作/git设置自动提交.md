### 1. windows 自动提交脚本 

autocommit.bat

```bat
@echo off
@title bat execute git auto commit

cd D:\phpStudy\WWW\s73\exercise\PHP Review\html\demo12\testgit\lianshou
git add .
git commit -m "Auto commit."
git push origin master
```

### 2. windows自动拉取

autopull.bat

```bat
@echo off
@title bat execute git auto pull

cd D:\phpStudy\WWW\s73\exercise\PHP Review\html\demo12\testgit\lianshou
git pull origin master
```



### 3. 简易的命令行入门教程:

```shell
Git 全局设置:
git config --global user.name "Ultron"
git config --global user.email "2548880572@qq.com"

创建 git 仓库:
mkdir typora
cd typora
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin git@gitee.com:Ultron2018/typora.git
git push -u origin master


已有仓库?

cd existing_git_repo
git remote add origin git@gitee.com:Ultron2018/typora.git
git push -u origin master

或者github 上的提交

git "# backgroundManager" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin git@github.com:goalwaybe/www.qiyesix.com.git
git push -u origin main
```



