### 一， composer 安装laravel

1.比如安装一个shop的项目

#composer create-project laravel/laravel --prefer-dist shop

2.下载其他版本的

注意，如果要下载其他版本， 比如5.4版本中的最小版本号 ， 可以使用这个命令

composer create-project laravel/laravel=5.4.* --prefer-dist  ./

composer create-project --prefer-dist laravel/laravel blog 5.5.*

### 二， 直接从laravel学院进行下载

百度搜索直接下载压缩包

### 三，项目下载速度慢问题

中国镜像, 原生composer 镜像都是很慢, 下载一半会报错

使用阿里云composer镜像,问题解决

```shell
composer config repo.packagist composer https://mirrors.aliyun.com/composer/
```

### 四, 关于安装完成后, 运行报错问题

开启.env错误调试,得知是缺少key

php artisan key:generate