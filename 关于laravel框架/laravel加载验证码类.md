如何加载验证码类呢

### 一.   验证码类存在于 App\Org\code\Code.class.php

```json
 "autoload": {
        "classmap": [
            "database/seeds",
            "database/factories"
        ],
        "psr-4": {
            "App\\": "app/"
        },
        "files": ["App/Org/code/Code.class.php"]
    }
```



```cmd
composer dump-autoload
```

### 二.  怎么使用

```php
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Org\code\Code;

class LoginController extends Controller
{
    //后台登录页
    public function login(){
        return view('admin.login');
    }

    //验证码
    public function code(){
        $code = new Code();
        return $code->make();
    }

}
```

