### 一, 常见命令行

创建项目

```
composer create-project laravel/laravel=5.4.* --prefer-dist ./目录名字
```

查看路由列表

```shell
php artisan route:list
```

创建控制器

```
php artisan make:controller TestController
```

控制器下创建分目录

```
php artisan make:controller Admin/IndexController
```

```
php artisan make:controller Home/IndexController
```

控制器模型一起建立

```
php artisan make:controller Admin/NodeController -r -m Models/Node  注入模型到编辑修改中


```



app目录下创建模型

```
php artisan make:model Admin/Member
```

laravel 加载语言包

```
composer require caouecs/laravel-lang:~3.0
```

安装验证码

```
composer require mews/captcha
composer require "mews/captcha:~2.0"

安装验证码注册服务
config/app.php
 //mews captcha 服务提供者
 Mews\Captcha\CaptchaServiceProvider::class,
 别名
 //验证码别名
 'Captcha' => Mews\Captcha\Facades\Captcha::class,
安装验证码包后生成默认配置
php artisan vendor:publish

```

### 二, composer 安装指定版本

```
composer require "foo/bar:1.0.0"
composer require "mews/captcha:~2.0"
```

### 三,数据库迁移文件

```
php artisan make:migration create_paper_table
//执行迁移文件
php artisan migrate:install
//数据库生成迁移文件后的生成表
php artisan migrate
//回滚
php artisan migrate:rollback
//填充器
php artisan make:seeder PaperTableSeeder
//执行填充
php artisan db:seed --class=PaperTableSeeder

模型工厂
大批量数据填充
php artisan make:factory PaperFactory
php artisan migrate:refresh
php artisan db:seed --class=PaperTableSeeder
```

四, 安装debugbar 调试工具条

```
composer require barryvdh/laravel-debugbar:~2.4
```

