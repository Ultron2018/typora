### 一. 压力测试



> 运行：
> 在Windows系统下，打开cmd命令行窗口，定位到apache安装目录的bin目录下
> cd C:\Program Files (x86)\Apache Software Foundation\Apache2.2\bin
>
> 键入命令：
> ab -n 800 -c 800 http://192.168.0.10/
> （-n发出800个请求，-c模拟800并发，相当800人同时访问，后面是测试url）
>
> ab -t 60 -c 100 http://192.168.0.10/
> 在60秒内发请求，一次100个请求。
>
> //如果需要在url中带参数，这样做
> ab -t 60 -c 100 -T "text/plain" -p p.txt http://192.168.0.10/hello.html
> p.txt 是和ab.exe在一个目录
> p.txt 中可以写参数，如 p=wdp&fq=78

