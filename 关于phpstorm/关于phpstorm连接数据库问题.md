

### 1.PHPstorm连接数据库失败；MySQL数据库时区更改；MySQL不是内部命令

PHPstorm连接数据库报（Server returns invalid timezone. Go to ‘Advanced’ tab and set ‘serverTimezone’ property manually）错误，MySQL默认的时区是UTC时区，默认为0区，北京是东八区，比北京晚了8个小时，所以要修改MySQL的时长
————————————————
版权声明：本文为CSDN博主「qq_40922915」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/qq_40922915/article/details/105892841

#### 第一步：打开cmd，输入以下命令

```
mysql -hlocalhost -uroot -p

```





#### 第二步：输入MySQL的数据库连接密码，执行以下命令

```
set global time_zone='+8:00';

```

