### 1.postman 的设置环境变量测试jwt 接口

postman环境和全局变量设置语句

•postman.setEnvironmentVariable(variableName, variableValue) ：设置一个环境变量“variableName”，并为 ，并为

其分配字符串 其分配字符串“variableValue”。您必须 。您必须为此方法选择一个环境才能工作。 为此方法选择一个环境才能工作。 注意：只能存储字符串。存储其他类型 注意：只能存储字符串。存储其他类型

的数据将导致意外的行为。

•postman.getEnvironmentVariable(variableName) ：返回环境变量“variableName”的值，用于预先请求和测试 的值，用于预先请求和测试

脚本。您必须 为此方法选择一个环境才能工作。

•postman.setGlobalVariable(variableName, variableValue) ：设置一个全局变量“variableName”，并为其分配 ，并为其分配

字符串 字符串“variableValue” 。 注意：只能存储字符串。存储其他类型的数据将导致意外的行为。 注意：只能存储字符串。存储其他类型的数据将导致意外的行为。

•postman.getGlobalVariable(variableName) ：返回全局变量“variableName”的值，用于预请求和测试脚本。 的值，用于预请求和测试脚本。

•postman.clearEnvironmentVariable(variableName) ：清除名为“variableName”的环境 的环境 变量。您必须 为此方法

选择一个环境才能工作。

•postman.clearGlobalVariable(variableName) ：清除名为“variableName”的全局变量。 的全局变量。

•postman.clearEnvironmentVariables()：清除所有环境 ：清除所有环境 变量。您必须 为此方法选择一个环境才能工作。

•postman.clearGlobalVariables()：清除所有全局变量。 ：清除所有全局变量。

•environment ：当前环境中的变量字典。使用 environment["foo"] 访问“foo”的环境变量的值。注意：这只能用 的环境变量的值。注意：这只能用

于读取变量。使用 于读取变量。使用 setEnvironmentVariable() 设置值。 设置值。

•globals 全局变量字典 使用 globals["bar"] 访问“ 栏中的”全局变量的值。注意：这只能用于读取变量。使用 全局变量的值。注意：这只能用于读取变量。使用

setGlobalVariable() 设置值

#### 2.相关 Tests设置脚本 

```
 var data = JSON.parse(responseBody);

if (data.data.token) {
  	tests["Body has token"] = true;
		postman.setEnvironmentVariable("token", '434343');  //设置环境变量, 需要选中环境
	    postman.setGlobalVariable("token", data.data.token)   //设置全局变量
	}else {
	  	tests["Body has token"] = false;
	 }
```



```
var data = JSON.parse(responseBody);

if (data.data.token) {
  		tests["Body has token"] = true;
		postman.setEnvironmentVariable("token", '434343');
	    postman.setGlobalVariable("token",data.data.token);
	}else {
	  	tests["Body has token"] = false;
	 }
	 

使用的时候使用{{token}}  , {{变量名字}}
```





#### 3.参考网络资料

 https://blog.csdn.net/qq_30036559/article/details/79462522?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param 





### 二, PHP JWT简易使用

https://blog.csdn.net/weixin_45100995/article/details/114268847?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-2&spm=1001.2101.3001.4242