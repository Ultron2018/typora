### 一,问题描述

最近进行 Thinkphp5.1 框架的后台设计时，发现使用默认的图片上传功能，保存的图片路径中有一个反斜线"\"
    在浏览器中打开（windows下），图片访问自然是正常的
    但是，在 app 获取路径显示时，是无法识别这种反斜线"\"的

### 二,解决方案

- 在使用默认的上传方法时，注意字符串替换即可

```php
 *//把反斜杠(\)替换成斜杠(/) 因为在windows下上传路是反斜杠径
$getSaveName=str_replace("\\","/",$info->getSaveName()); 

$fileUrl = '/upload/'.$getSaveName; 
```

![](https://img-blog.csdnimg.cn/20190315160138546.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE0MTU3ODI=,size_16,color_FFFFFF,t_70)

参考文章: https://blog.csdn.net/u011415782/article/details/88576727