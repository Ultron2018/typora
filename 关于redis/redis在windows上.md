### 一, redis连接

redis连接格式为 redis-cli -h host -p port -a password,但由于刚安装的redis是没有密码的,因此可以进行直接连接，cd转到redis目录里



```
redis-cli -h 127.0.0.1 -p 6379

因为是在本地，所以也可以这样写
redis-cli.exe -h 127.0.0.1 -p 6379
```

查看当前密码

```
config get requirepass
127.0.0.1:6379> config get requirepass
1) "requirepass"
2) ""

当下，你可以选择设置一次性密码，一次性密码则是：当redis重启后将失效密码,连接上

D:\Soft\Redis-x64-3.0.504>redis-cli -h 127.0.0.1 -p 6379 -a  123456
127.0.0.1:6379>
或者设置redis重启也不失效的密码


```

###### 1：一次性密码

可通过命令直接修改

```
config set requirepass "123456"

127.0.0.1:6379> config get requirepass
1) "requirepass"
2) "123123"
```

###### 2:设置永久性密码

找到redis的配置文件 redis.windows.conf, 打开，找到 requirepass 直接修改为自己想要的密码, 修改完后重启 redis 即可生效



